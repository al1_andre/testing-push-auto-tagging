# Environnment variables

 - PRIVATE_TOKEN : Create your personnal [access token](/profile/personal_access_tokens)
 - MILESTONE : The milestone you whant to close (default is defined in `.gitlab-ci` with '1.0.0')

# CI
When pushing to the *beta* or *master* branch, the CI will close all issues referenced in the commits of the given milestone (defined in `.gitlab-ci.yml`) and add a label wich can be *beta* or the committed tag.
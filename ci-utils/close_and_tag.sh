#!/usr/bin/env bash

# Get opened issues of the defined milestone
request=$(curl -s -H "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/issues?milestone=$MILESTONE&state=opened")
iids=($(jq -r '.[].iid' <<< $request))
labels=($(jq -cr '.[].labels' <<< $request))

# Update issues with the commit tag and close if they are present in git commits
for index in "${!iids[@]}"; do
  gitlog=$(git log --pretty=format:"%s" --grep="#${iids[$index]}")
  labels=$(sed -r 's/\[|\]|"//g' <<< ${labels[$index]})",$CI_COMMIT_TAG"
  if [[ ! -z $gitlog ]]; then
    echo "Tagging $labels and closing issue : $gitlog"
    tagging=$(curl -s -X PUT -H "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/issues/${iids[$index]}?labels=$labels&state_event=close")
  fi
done
